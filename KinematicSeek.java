

public class KinematicSeek {
	private Vector2D character;
	private Vector2D target;
	private Vector2D maxSpeed;
	
	
	public KinematicSeek() {
		super();
	}


	public KinematicSeek(Vector2D character, Vector2D target, Vector2D maxSpeed) {
		super();
		this.character = character;
		this.target = target;
		this.maxSpeed = maxSpeed;
	}


	public Vector2D getCharacter() {
		return character;
	}


	public void setCharacter(Vector2D character) {
		this.character = character;
	}


	public Vector2D getTarget() {
		return target;
	}


	public void setTarget(Vector2D target) {
		this.target = target;
	}


	public Vector2D getMaxSpeed() {
		return maxSpeed;
	}


	public void setMaxSpeed(Vector2D maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	
	private SteeringOutput getSteering()
	{
		return new SteeringOutput();
	}
	
}
